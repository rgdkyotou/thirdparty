#!/bin/bash
_src=$1
_ver=$2
_uid=$3
. scripts/oneline.sh
	echo "Fail" > $_stat
selinuxenabled
if [ $? -eq 0 ];then
	echo "SELINUXが有効です. 継続できません."
	exit	3
fi
if [ `systemctl is-enabled mariadb` != "enabled" ];then
	cat<<@
ジョブ管理サーバーを準備する前に, MariaDBを設定する必要があります. 以下では, MariaDBの管理者パスワードを設定してください.
@
	
	exit
fi
if [ ! -d /var/spool/slurmd ];then
	mkdir	/var/spool/slurmd
fi
if [ ! -d /var/log/slurmd ];then
	mkdir	/var/log/slurmd
fi
if [ ! -d /var/log/slurm ];then
	mkdir	/var/log/slurm
fi
if [ ! -d /var/slurmd ];then
	mkdir	/var/slurmd
fi
find_uid(){
	getent passwd slurm	>	/dev/null
	if [ $? -ne 0 ];then
		slurm_id=${1:-666}
		while [ `getent passwd $slurm_id | wc -l ` -ne 0 -a `getent group $slurm_id | wc -l ` -ne 0 ];do
			(( slurm_id = slurm_id + 1 ))
		done
		echo "Making user slurm [$slurm_id]"
		useradd -c "SLURM-DAEMON" -M -r -u $slurm_id  -U slurm
	else
		slurm_id=`getent passwd slurm|cut -f3 -d:`
		echo "Fond user slurm [$slurm_id]"
	fi
	echo $slurm_id > rc/slurm.uid
}
find_uid								2>&1	|oneline
chown slurm.slurm		/var/spool/slurmd /var/slurmd /var/log/slurm
cp rc/slurm.uid			/usr/local/etc/slurm.uid
cp rc/slurm.conf		/usr/local/etc/slurm.conf
cp rc/slurmdbd.sysconfig	/etc/sysconfig/slurmdbd
cp rc/slurmctld.sysconfig	/etc/sysconfig/slurmctld
cp rc/slurm.tmpfiles.conf	/etc/tmpfiles.d/slurm.conf
cp rc/slurm.ldconfig		/etc/ld.so.conf.d/slurm.conf
ldconfig
echo "Configure SlurmDbd..."						2>&1	|oneline
cp rc/slurmdbd.conf					/usr/local/etc
PIDFILE=`grep ^PidFile rc/slurmdbd.conf|cut -f2 -d=`
sed	-e 's@PIDFile=.*@PIDFile='${PIDFILE}'@'	\
	CLUSTER/BASE/slurm/etc/slurmdbd.service		> /etc/systemd/system/slurmdbd.service
systemctl daemon-reload
systemctl stop slurmdbd
#firewall is already disabled
#firewall-cmd --add-port=1234/tcp --permanent				2>&1	|oneline
#firewall-cmd --add-port=6819/tcp --permanent				2>&1	|oneline
echo "Configure SlurmCtld..."						2>&1	|oneline
PIDFILE=`grep SlurmctldPidFile rc/slurm.conf|cut -f2 -d=`
sed	-e 's@PIDFile=.*@PIDFile='${PIDFILE}'@'\
	-e '/^ExecReload/iUser=slurm'		\
	-e '/^ExecReload/iGroup=slurm'		\
	CLUSTER/BASE/slurm/etc/slurmctld.service	> /etc/systemd/system/slurmctld.service
systemctl daemon-reload
systemctl stop slurmctld
#firewall is already disabled
#firewall-cmd --add-port=6817/tcp --permanent				2>&1	|oneline
#firewall-cmd --reload							2>&1	|oneline
if [ -d /var/slurmd ];then
	rm -rf /var/slurmd/*
fi
systemctl start slurmdbd
	if [ ${PIPESTATUS[0]} -ne 0 ];then
		echo "FAIL slurmdbd" > $_stat;
		systemctl status slurmdbd
		exit;
	fi
systemctl start slurmctld
	if [ ${PIPESTATUS[0]} -ne 0 ];then
		echo "FAIL slurmctld" > $_stat;
		systemctl status slurmctld
		exit;
	fi
systemctl enable slurmdbd
systemctl enable slurmctld
echo "Building Database..."						2>&1	|oneline
CLUSTER_NAME=`grep ClusterName rc/slurm.conf|cut -f2 -d=`
echo "sacctmgr create cluster $CLUSTER_NAME"
sacctmgr create cluster $CLUSTER_NAME
	if [ ${PIPESTATUS[0]} -ne 0 ];then
		echo "FAIL create cluster" > $_stat;
		sacctmgr show cluster
		exit;
	fi
	echo "ジョブかきん・かんりサーバOK"		2>&1	|oneline
	echo "OK $_ver" > $_stat
