#!/bin/bash
#	どうもgitでインストールすると, ライブラリとヘッダーファイルのバージョンが
#	ずれちまって不安だ
if [ ${2##*.} == "git" ];then
	_repo=$2
	_ver=$3
	_src=`basename $_repo .git`
else
	_url=$2
	_ext=$3
	_stat=$4
	_src=`basename $_url .$_ext`
	_ver=${_src#$4-*}
fi
. scripts/oneline.sh
pushd $1						>&	/dev/null
if [ "$_repo" != "" ];then
	git_download $_repo $_ver
	pushd $_src						>&	/dev/null
else
	wget_download $_url
	pushd $_src						>&	/dev/null
fi
cp tools/build/example/user-config.jam tools/build/src
echo "using mpi : mpic++ ; "	>>	tools/build/src/user-config.jam
echo "using python : 3.6 : /usr/bin/python3.6 : /usr/include/python3.6m :/usr/lib/python3.6  ; "\
				>>	tools/build/src/user-config.jam
./bootstrap.sh --prefix=$_prefix			2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
./b2 --prefix=$_prefix					\
	 --without-mpi 					\
	link=shared					\
	install						2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then
	tail -3 $_log
	echo "ぶうすと: いくつかびるどできん"		2>&1	|oneline
	echo "OK $_ver" > $_stat
else
	echo "ぶうすとOK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
fi
