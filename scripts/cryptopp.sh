#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
pushd $_src						>&	/dev/null
gmake dynamic PREFIX=$_prefix	 2>&1 |oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
gmake install PREFIX=$_prefix	 2>&1 |oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "くりぷとぴぃぴぃOK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
