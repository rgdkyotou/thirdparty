#!/bin/bash
echo "Fail"		>	stat/$1
DST=/usr/local/bin
fix_dnfconf(){
	_RC=/etc/dnf/dnf.conf
	grep -q ^excludepkgs=munge $_RC
	if [ $? -ne 0 ];then
		echo "excludepkgs=munge*" >> $_RC
	fi
}
fix_resolver(){
	_RC=/etc/resolv.conf
	grep -q ^nameserver\ *127.0.0.1 $_RC
	if [ $? -ne 0 ];then
		grep -q ^nameserver\ *10.249.229.111 $_RC
		if [ $? -ne 0 ];then
			cat >> $_RC <<@
nameserver 10.249.229.111
@
		fi
		grep -q ^nameserver\ *10.249.229.223 $_RC
		if [ $? -ne 0 ];then
			cat >> $_RC <<@
nameserver 10.249.229.223
@
		fi
	fi
}
do_copy(){
	set -e
	git pull		>&	/dev/null
	cp	bin/smgr	$DST
	cp	bin/sj		$DST
	cp	bin/sl		$DST
	cp	bin/sn		$DST
	cp	rc/findsrv	$DST
	set +e
}
set_daily(){
	(( HOUR = RANDOM * 24 / 32767 ))
	(( MIN = RANDOM * 60 / 32767 ))
	TF=`mktemp`
	crontab -l > $TF
	echo "$MIN $HOUR * * * make -C `pwd` daily >& /dev/null" >>	$TF
	crontab < $TF
}
is_daily(){
	crontab -l | grep -qe "make -C `pwd` daily"
	ST=( ${PIPESTATUS[@]} )
	if [ ${ST[0]} -ne 0 ];then
		echo|crontab
	fi
	if [ ${ST[1]} -ne 0 ];then
		set_daily
	fi
}
tmp_clean(){
	if [ -d /local/disk0/tmp ];then
		(
		cd /local/disk0/tmp
		find . -mtime +30 -exec rm -rf {} \; 
		)
	else
		mkdir -p /local/disk0/tmp
		chmod og+rwx /local/disk0/tmp
	fi
}
do_copy
#is_daily
#fix_resolver
#fix_dnfconf
tmp_clean
echo "OK 2021-12-23"	>	stat/$1
