#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
pushd $_src						>&	/dev/null
if [ ! -d build ];then
	mkdir	build
fi
if [ ! -d build/release ];then
	mkdir	build/release
fi
if [ -f Makefile ];then
	make  	distclean				>&	/dev/null
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL clean" > $_stat;exit;fi
fi
cd build/release					>&	/dev/null
cmake\
	-DCMAKE_BUILD_TYPE=Release\
	-DBUILD_SHARED_LIBS=ON\
	-DCMAKE_INSTALL_PREFIX=$_prefix\
	-DCMAKE_INSTALL_LIBDIR=lib\
	-DWITH_BLAS=ON\
	-DWITH_Eigen3=ON -DEIGEN3_INCLUDE_DIR=$_prefix/include\
	-DWITH_GMPXX=ON\
	-DWITH_NTL=ON\
	-DWITH_ZLIB=ON -DZLIB_INCLUDE_DIR=/usr/include\
	 ../..			2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make							2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make install						2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "しーがるOK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
