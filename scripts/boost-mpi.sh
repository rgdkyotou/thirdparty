#!/bin/bash
_repo=$2
_ver=$3
_src="boost-mpi"
. scripts/oneline.sh
echo "Fail" > $_stat
pushd $1						>&	/dev/null
pushd boost						>&	/dev/null
cp tools/build/example/user-config.jam tools/build/src
echo "using mpi : mpic++ ;"	>>	tools/build/src/user-config.jam
echo "using python : 3.6 : /usr/bin/python3.6 : /usr/include/python3.6m :/usr/lib/python3.6  ;"\
				>>	tools/build/src/user-config.jam
./b2 --prefix=$_prefix					\
	 --with-mpi 					\
	link=shared					\
	install						2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then
	tail -3 $_log
	echo "ぶうすと: いくつかびるどできん"		2>&1	|oneline
	echo "Fail" > $_stat
else
	echo "ぶうすとえむぴあいOK"			2>&1	|oneline
	echo "OK $_ver" > $_stat
fi
