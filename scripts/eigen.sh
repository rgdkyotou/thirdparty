#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
pushd $_src						>&	/dev/null
install -o root -g root -d $_prefix/include/Eigen
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install(1)" > $_stat;exit;fi
find Eigen -type d -exec install -o root -g root -d $_prefix/include/{} \; 
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install(2)" > $_stat;exit;fi
find Eigen -type f -exec install -o root -g root {} $_prefix/include/{} \; 
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install(3)" > $_stat;exit;fi
cp signature_of_eigen3_matrix_library $_prefix/include/
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install(4)" > $_stat;exit;fi
echo "えいげんOK"					2>&1	|oneline
echo "OK $_ver"						>	$_stat
