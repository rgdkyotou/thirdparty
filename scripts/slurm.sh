#!/bin/bash
#	build CLUSTER/LIB/VTK-8.2.tar.gz tar.gz VTK
#
_repo=$2
_ver=$3
_src=`basename $_repo`
_src=${_src%.*}
. scripts/oneline.sh
	echo "Fail" > $_stat
pushd $1					>&	/dev/null
if [ ! -d $_src ];then
	git clone $_repo			2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
	pushd $_src				>&	/dev/null
	git checkout -b $_ver refs/tags/$_ver	2>&1	|oneline
	patch -p0 < ../pmixp_client.patch	2>&1	|oneline
	popd					>&	/dev/null
fi
echo "Using python3 as default"				|oneline
alternatives --set python /usr/bin/python3
cd $_src
if [ -f Makefile ];then
	make  	distclean	2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL clean" > $_stat;exit;fi
fi
_CMD="./configure --prefix=$_prefix --with-munge --with-pmix=$_prefix/pmix"
echo "$_CMD"		 2>&1	|oneline
eval $_CMD 		 2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make 	 		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make  install		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
popd						>&	/dev/null
#set -x
cp bin/smgr						/usr/local/bin/smgr
cp bin/sl						/usr/local/bin/sl
cp bin/sj						/usr/local/bin/sj
cp bin/sn						/usr/local/bin/sn
cp bin/sq						/usr/local/bin/sq
if [ ! -h /usr/local/bin/sb ];then
	ln -s	/usr/local/bin/sbatch	/usr/local/bin/sb
fi
if [ ! -h /usr/local/bin/sc ];then
	ln -s	/usr/local/bin/scancel	/usr/local/bin/sc
fi
	echo "すらるむOK" 			2>&1	|oneline
	echo "OK $_ver" > $_stat
#set +x
