#!/bin/bash
#	ビルドばslurm.shでしたけん，こいつでクライアントば設定すっと
_src=$1
_ver=$2
. scripts/oneline.sh
	echo "Fail" > $_stat
if [ ! -f rc/slurm.uid ];then
	make munge.key.copy
fi
getent passwd slurm	>	/dev/null
if [ $? -eq 0 ];then
	slurm_id=`getent passwd slurm|cut -f3 -d:`
	if [ $slurm_id != `cat rc/slurm.uid` ];then
		echo "ローカルのslurmユーザー[`getent password slurm`]が矛盾しちょるけえ削除しいさ"
		exit	3
	fi
else
	slurm_id=`cat rc/slurm.uid`
	echo "Making user slurm [$slurm_id]"
	useradd -c "SLURM-DAEMON" -M -r -u $slurm_id  -U slurm
fi
if [ ! -d /var/spool/slurmd ];then
	mkdir			/var/spool/slurmd
	chown slurm.slurm	/var/spool/slurmd
fi
if [ ! -d /var/log/slurmd ];then
	mkdir	/var/log/slurmd
	chown slurm.slurm	/var/log/slurmd
fi
if [ ! -d /var/slurmd ];then
	mkdir			/var/slurmd
	chown slurm.slurm	/var/slurmd
fi
if [ ! -f /etc/sysconfig/slurmd ];then
	cp rc/slurmd.sysconfig		/etc/sysconfig/slurmd
fi
if [ ! -f /usr/local/etc/cgroup.conf ];then
	cp rc/cgroup.conf		/usr/local/etc
fi
if [ ! -f /etc/ld.so.conf.d/slurm.conf ];then
	cp rc/slurm.ldconfig		/etc/ld.so.conf.d/slurm.conf
	ldconfig
fi
cp rc/slurm.conf			/usr/local/etc/slurm.conf
#TMPFS must exist
FSLOC=`grep ^TmpFS /usr/local/etc/slurm.conf|cut -f2 -d=`
if [ ! -d $FSLOC ];then
	mkdir -p $FSLOC
fi
#Starting SLURMD
PIDFILE=`grep SlurmdPidFile rc/slurm.conf|cut -f2 -d=`
sed -e 's@PIDFile=.*@PIDFile='${PIDFILE}'@'\
	CLUSTER/BASE/slurm/etc/slurmd.service	> /etc/systemd/system/slurmd.service
systemctl daemon-reload
#firewall-cmd --add-port=6818/tcp --permanent				2>&1	|oneline
#firewall-cmd --reload							2>&1	|oneline
systemctl enable slurmd
	echo "じっこーのーどOK"						2>&1	|oneline
	echo "OK $_ver" > $_stat
echo "$(tput setaf 1)$(tput setab 7)Please start slurmd by [ systemctl start slumd ] AFTER YOU CONFIRM slurmd.conf$(tput sgr0)"
echo "$(tput setaf 1)$(tput setab 7)that must include this machine as $(tput sgr0)"
/usr/local/sbin/slurmd -C
echo -e "TmpDisk=\c"
df -B1048576 --output=avail $FSLOC|tail -1
echo "$(tput setaf 1)$(tput setab 7)or you can use [ smgr ] to apply [c] command$(tput sgr0)"
