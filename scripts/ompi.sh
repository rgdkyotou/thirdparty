#!/bin/bash
download(){
	_src=$1
	. scripts/oneline.sh
		echo "Fail" > $_stat
	dnf install -y openmpi openmpi-devel					2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL dnf" > $_stat;exit;fi
	cp rc/openmpi.ldconfig					/etc/ld.so.conf.d/openmpi.conf
	cp rc/openmpi.profile					/etc/profile.d/openmpi.sh
	echo "Disable firewall for MPI nodes...."				2>&1	|oneline
	systemctl disable firewalld
	systemctl stop firewalld
		echo "えむぴーあいOK"						2>&1	|oneline
		echo "OK" > $_stat
}
build(){
	_repo=$2
	_ver=$3
	_src=`basename $_repo`
	_src=${_src%.*}
	. scripts/oneline.sh
	echo "Disable firewall for MPI nodes...."	2>&1	|oneline
	systemctl disable firewalld			2>&1	|oneline
	systemctl stop firewalld			2>&1	|oneline
	echo "[DOWNLOAD]"				2>&1	|oneline
	echo "Fail" > $_stat
	pushd $1					>&	/dev/null
	if [ ! -d $_src ];then
		git clone $_repo			2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
		pushd $_src				>&	/dev/null
		git checkout -b $_ver refs/tags/$_ver	2>&1	|oneline
		popd 					>&	/dev/null
	fi
	pushd $_src				>&	/dev/null
	if [ -f Makefile ];then
		make  	distclean		>&	/dev/null
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL clean" > $_stat;exit;fi
	fi
	echo -e "\n[AUTOGEN]"				2>&1	|oneline
	./autogen.pl					2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL automake" > $_stat;exit;fi
	_CMD="./configure --prefix=$_prefix --with-slurm --with-pmix=$_prefix/pmix --with-libevent --with-hwloc=internal --with-ompi-pmix-rte --disable-mca-dso"
	echo -e "\n[CONFIGURE]"				2>&1	|oneline
	echo -e "\n${_CMD:0:$_width}" 			2>&1	|oneline
	echo -e "\n${_CMD:$_width}" 			2>&1	|oneline
	eval $_CMD					2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
	make							2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
	make install						2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	popd 							>&	/dev/null
	popd 							>&	/dev/null
	cp rc/openmpi.ldconfig					/etc/ld.so.conf.d/openmpi.conf
	cp rc/openmpi.profile					/etc/profile.d/openmpi.sh
	ldconfig
		echo "えむぴーあいOK"						2>&1	|oneline
		echo "OK $_ver" > $_stat
}
build $*
