#!/bin/bash
echo "Fail"		>	stat/$1
CONF=/etc/selinux/config
_VAL=`grep ^SELINUX=  $CONF|cut -d= -f2`
if [ $_VAL != disabled ];then
	sed -e 's/^SELINUX=.*/SELINUX=disabled/' -i".org" $CONF
	setenforce 0
fi
echo "OK disabled"	>	stat/$1
