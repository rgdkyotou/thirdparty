#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
pushd $_src						>&	/dev/null
cp mpreal.h /usr/local/include
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "$_src OK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
