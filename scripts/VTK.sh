#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
_build=$_src.build
if [ ! -d $_build ];then mkdir $_build; fi
pushd $_build						>&	/dev/null
if [ -f Makefile ];then
	make distclean					>&	/dev/null
fi
cmake	-DCMAKE_BUILD_TYPE=Release\
	-DBUILD_SHARED_LIBS=ON\
	-DCMAKE_INSTALL_PREFIX=$_prefix\
	-DCMAKE_INSTALL_LIBDIR=lib\
	../$_src	2>&1 | oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make			2>&1 |oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make install		2>&1 |oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "ゔってけOK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
