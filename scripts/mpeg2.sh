#!/bin/bash
_url=$2
_ext=$3
_stat=$4
_src=`basename $_url .$_ext`
_ver=${_src#$4-*}
. scripts/oneline.sh
pushd $1						>&	/dev/null
wget_download $_url
#cd $_src
cd mpeg2
make  all	2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
cp	src/mpeg2enc/mpeg2encode	/usr/local/bin		2>&1	|oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
echo "`basename $_stat`:OK"					2>&1	|oneline
echo "OK $_ver" > $_stat
