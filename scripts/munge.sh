#!/bin/bash
#
download(){
	_src=$1
	. scripts/oneline.sh
		echo "Fail" > $_stat
	dnf config-manager --enable powertools
	dnf install -y munge munge-devel	2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL dnf" > $_stat;exit;fi
	cp rc/munge.key	/etc/munge/munge.key				
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL key" > $_stat;exit;fi
	chmod 400		/etc/munge/munge.key
	chown munge 		/etc/munge/munge.key
	systemctl stop munge						>&	/dev/null
	systemctl enable munge						>&	/dev/null
	systemctl start munge						>&	/dev/null
		if [ ${PIPESTATUS[0]} -ne 0 ];then
			echo "FAIL start" > $_stat;
			systemctl status munge
			exit;
		fi
	_RPM=( `dnf list installed ${@:$#:1}|tail -1` )
	echo "OK ${_RPM[1]}" > $_stat
}
my_build(){
	. scripts/opt.sh $1 $2 $3
	pushd $_dir							>&	/dev/null
	if [ ! -d $_src ];then unpack $_file; fi
	cd $_src
	./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --with-crypto-lib=openssl 2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
	make  		2>&1 |oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
	make  install	2>&1 |oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	popd
	cp rc/munge.key /etc/munge/munge.key
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL key" > $_stat;exit;fi
	chmod 400 /etc/munge/munge.key
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL chmod" > $_stat;exit;fi
		echo "OK" > $_stat
}
download $*
