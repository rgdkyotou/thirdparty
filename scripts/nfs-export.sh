#!/bin/bash
#	$0	statfile ethername
#	e.g.	nfs-export.sh nfs-export bond0
_src=$1
. scripts/oneline.sh

SLURM_CFG=/usr/local/etc/slurm.conf
EXPORTS=/etc/exports
FSTABLE=/etc/fstab
EXPORT_DIR=/export

if [ -f $SLURM_CFG ];then
	FSLOC=`grep ^TmpFS $SLURM_CFG|cut -f2 -d=|cut -f1 -d\ `
else
	FSLOC=/local/disk0
fi
NET=( `ip addr show $2|grep inet|grep -v inet6` )
IP=${NET[1]}

make_export(){
	grep -q $EXPORT_DIR'[[:space:]]' $EXPORTS
	if [ $? -eq 1 ];then
		echo "$EXPORT_DIR $IP(rw,fsid=0) localhost(rw,fsid=0)"
	fi
	grep -q $EXPORT_DIR/disk0'[[:space:]]' $EXPORTS
	if [ $? -eq 1 ];then
		echo "$EXPORT_DIR/disk0 $IP(rw,nohide) localhost(rw,nohide)"
	fi
	if [ ! -d $EXPORT_DIR/disk0 ];then
		mkdir -p $EXPORT_DIR/disk0
	fi
}
make_fstab(){
	grep -q ^$FSLOC'[[:space:]]' $FSTABLE
	if [ $? -eq 1 ];then
		echo "$FSLOC $EXPORT_DIR/disk0 none bind 0 0"
		echo "$FSLOC /disk0 none bind 0 0"
	fi
	if [ ! -d /disk0 ];then
		mkdir -p /disk0
	fi
}

do_work(){
	cat $FSTABLE
	mount -a			
	dnf install -y nfs-utils
	exportfs -a					
	systemctl enable nfs-server		
	systemctl start nfs-server			
	systemctl enable autofs
	systemctl start autofs
	systemctl reload autofs
}

if [ ! -f $EXPORTS ];then touch $EXPORTS;fi
make_export		>>	$EXPORTS
make_fstab		>>	$FSTABLE
do_work	2>&1		|oneline
echo "えねふすOK"	2>&1	|oneline
echo "OK $EXPORT_DIR" 	> $_stat
