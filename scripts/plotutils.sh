#!/bin/bash
_repo=$2
_ver=$3
_stat=$4
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
cd $_src
cd plotutils-2.6
#このやろう libpng でバグってる
export LC_ALL=C LANG=C
./configure --prefix=$_prefix --without-libpng 	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make  						2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make  install					2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
ldconfig
echo "`basename $_stat`:OK"			2>&1	|oneline
echo "OK $_ver" > $_stat
