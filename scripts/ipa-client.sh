#!/bin/bash
_ether=$1
_src=$2
. scripts/oneline.sh
echo "Fail" > $_stat
#Check IP-ADDR HOSTNAME
_IP4=( `ip addr show $_ether|grep inet\ ` )
_IP4N=${_IP4[1]%/*}
_H4=( `host $_IP4N` )
_HOSTNAME=${_H4[${#_H4[@]}-1]%.}
echo "IPADDR[$_IP4N] HOSTNAME[$_HOSTNAME]"
#----Install
#dnf install perl -y								2>&1	|oneline
dnf module install -y idm:DL1					2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
#	なんかエラーするしうるさいし
rpm -q rasdaemon perl-DBD-SQLite perl-DBI		>&	/dev/null
if [ $? -eq 0 ];then
	rpm -e rasdaemon perl-DBD-SQLite perl-DBI
fi
sed -e '/\[main\]/adns=none' -i".org" /etc/NetworkManager/NetworkManager.conf
if [ `hostname` != $_HOSTNAME ];then
	echo "ホスト名[`hostname`]がDNS情報[$_HOSTNAME]と一致しません."
	echo "$(tput setaf 1)$(tput setab 7)訂正するには hostnamectl set-hostname $_HOSTNAME を実行するのぢゃ$(tput sgr0)"
	exit	4
fi
grep -q $_IP4N /etc/hosts
if [ $? -ne 0 ];then
	cat >> /etc/hosts <<@
$_IP4N	$_HOSTNAME
@
fi
#FIREWALL
firewall-cmd --add-service={freeipa-ldap,freeipa-ldaps,dns}  --permanent
firewall-cmd --reload
ipa-client-install --force-join
ipa-client-automount
_RPM=( `dnf list installed ipa-client|tail -1` )
#Disable local /net
sed -e 's/^\/net/#\/net/' -i".org" /etc/auto.master
#----Make alias
_STAT=1
while [ $_STAT -ne 0 ];do
	echo -e "お前は本名[`hostname -s`]に通名をつけよう(嫌なら本名):\c"
	read _ALIAS
	if [ "$_ALIAS" == "" ];then
		_ALIAS=`hostname -s`
	fi
	_DOMAIN=${_ALIAS#*.}
	if [ $_DOMAIN == $_ALIAS ];then
		_ALIAS=${_ALIAS}.`dnsdomainname`
	fi
	host ${_ALIAS} >& /dev/null 
	_STAT=$?
	if [ ${_STAT} -ne 0 ];then
		echo "$(tput setab 7)$(tput setaf 1)DNSに[${_ALIAS}]が登録されていないからダメ$(tput sgr0)"
		continue
	fi
	_LAST=( `host ${_ALIAS} | tail -1` )
	_NIP=${_LAST[${#_LAST[@]}-1]%.}
	if [ "$_NIP" != "$_IP4N" ];then
		echo "$(tput setab 7)$(tput setaf 1)そりゃ別のマシンの別名だろうが$(tput sgr0)"
		_STAT=1
	fi
done
if [ ${_ALIAS} != `hostname` ];then
	grep -q ${_ALIAS} /etc/hosts
	if [ $? -eq 1 ];then
		echo "$(tput setab 2)$_ALIAS を `hostname` の別名とする$(tput sgr0)"
		sed\
		-e '/^'$_IP4N'/s/$/ '$_ALIAS'/'	-i".org" /etc/hosts
		sed\
		-e '/^HOSTNAME=/aHOSTNAME_ALIAS=`/usr/bin/hostname -a 2>/dev/null`\nexport HOSTNAME_ALIAS=${HOSTNAME_ALIAS:-$HOSTNAME}'\
		-i".org" /etc/profile
		echo '[ "$PS1" ] && PS1="[\u@${HOSTNAME_ALIAS%%.*} \W]\\$ "' >> /etc/bashrc
		echo "/etc/hosts, /etc/bashrc, /etc/profile に追記しました:"
		grep $_ALIAS /etc/hosts
		grep HOSTNAME_ALIAS /etc/bashrc /etc/profile
	else
		echo "$(tput setab 2)登録済みでしたね$(tput sgr0)"
	fi
else
	echo "$(tput setab 2)では本名で$(tput sgr0)"
fi
	echo "OK ${_RPM[1]}" > $_stat
	echo "OK ${_RPM[1]}" |oneline
