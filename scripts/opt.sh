#!/bin/bash
#	ex.	opt.sh	CLUSTER/LIB/vtk.tar.gz	tar.gz VTK
if [ ! -f $1 ];then
	printf "\033[101mCan not find $1\033[0m"
	exit 1
fi
_file=`basename $1`
_dir=`dirname $1`
_ext=$2
_src=`basename $_file .$_ext`
. scripts/oneline.sh
