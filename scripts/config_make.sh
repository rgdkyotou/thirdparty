#!/bin/bash
if [ ${2##*.} == "git" ];then
	_repo=$2
	_ver=$3
	_src=`basename $_repo .git`
else
	_url=$2
	_ext=$3
	_stat=$4
	_src=`basename $_url .$_ext`
	_ver=${_src#$4-*}
fi
. scripts/oneline.sh
pushd $1						>&	/dev/null
if [ "$_repo" != "" ];then
	git_download $_repo $_ver
	cd $_src
	# How to make configure...?
else
	wget_download $_url
	cd $_src
fi
./configure --prefix=$_prefix	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make  		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make  install	2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
echo "`basename $_stat`:OK"					2>&1	|oneline
echo "OK $_ver" > $_stat
