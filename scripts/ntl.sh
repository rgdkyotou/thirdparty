#!/bin/bash
_url=$2
_ext=$3
_stat=$4
_src=`basename $_url .$_ext`
_ver=${_src#$4-*}
. scripts/oneline.sh
pushd $1						>&	/dev/null
wget_download $_url
#----
cd $_src
cd src
if [ -f Makefile ];then
	make distclean				2>&1|oneline
fi
chmod +x configure
./configure PREFIX=$_prefix GMP_PREFIX=/usr	2>&1|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL unpack" > $_stat;exit;fi
make clobber		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL clobber" > $_stat;exit;fi
make all		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make install		2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
echo "NTL:OK"					2>&1	|oneline
echo "OK $_ver"					 > $_stat
