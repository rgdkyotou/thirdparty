#!/bin/bash
#	build basefolder repositoy ID repository-name
. scripts/oneline.sh
_src=$4
_stat=`pwd`/stat/${3:-${_src}}
_log=`pwd`/log/${3:-${_src}}
pushd $1
if [ ! -d $4 ];then
	git clone $2				2>&1	|oneline
	echo "Setting pmix-version 2.1.1"	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
	pushd $4	>& /dev/null
	git checkout -b vvv refs/tags/v2.1.1	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL checkout" > $_stat;exit;fi
	popd	>& /dev/null
fi
cd $4
./autogen.pl		2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL automake" > $_stat;exit;fi
./configure		2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make			2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
cp -v	include/*.h	/usr/include		2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL chmod" > $_stat;exit;fi
	echo "OK" > $_stat
