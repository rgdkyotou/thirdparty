#!/bin/bash
#	$_src	-->  $_prefix, $_log, $_stat
if [ ! -d `pwd`/log ];then
	mkdir `pwd`/log
fi
if [ ! -d `pwd`/stat ];then
	mkdir `pwd`/stat
fi
_prefix=/usr/local
_log=`pwd`/log/${_src}
_name=${_stat:-$_src}
_stat=`pwd`/stat/${_name}
touch $_log
_columns=$(tput cols)
(( _width = _columns -20 ))
oneline(){
	while read line
	do
		echo -e "${_name} $line" >> $_log
		echo -e "$(tput setab 1)${_name::16}$(tput sgr0)$(tput setaf 2)${line::$_width}$(tput el)$(tput sgr0)\r\c"
	done
	echo
}
case "$_ext" in
zip)		unpack(){ unzip $_src.$_ext; };;
tar.bz2)	unpack(){ tar jxf $_src.$_ext; };;
tar.gz)		unpack(){ tar zxf $_src.$_ext; };;
tar.xz)		unpack(){ tar Jxf $_src.$_ext; };;
tar.lz)		unpack(){ tar --lzip -xf $_src.$_ext; };;
*)		unpack(){ return; };;
esac
git_download(){
	if [ "$1" == "" -o "$2" == "" ];then
		echo "git_download repository tag "	2>&1	|oneline
		exit
	fi
	if [ ! -d $_src ];then
		git clone --recursive $1			2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
		pushd $_src						>&	/dev/null
		git checkout -fb $2 refs/tags/$2 --recurse-submodules	2>&1	|oneline
		popd 							>&	/dev/null
	fi
}
wget_download(){
	_url=$1
	if [ ! -f $_src.$_ext ];then
		wget $_url					2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
	fi
	if [ ! -d $_src ];then
		unpack $_src.$_ext				2>&1	|oneline
		if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL unpack" > $_stat;exit;fi
	fi
}
echo "Fail" > $_stat
