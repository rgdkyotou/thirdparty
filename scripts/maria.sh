#!/bin/bash
_src=maria
. scripts/oneline.sh
dnf install -y mariadb-server mariadb-server-utils mariadb-backup mariadb | oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
grep -q socket= /etc/my.cnf.d/client.cnf
if [ $? -ne 0 ];then
	sed -e '/\[client\]/asocket=/var/lib/mysql/mysql.sock' -i".org" /etc/my.cnf.d/client.cnf
	sed 								\
		-e '/\[mysqld\]/ainnodb_buffer_pool_size=1024M'		\
		-e '/\[mysqld\]/ainnodb_log_file_size=64M'		\
		-e '/\[mysqld\]/ainnodb_lock_wait_timeout=900'		\
		-i".org"	/etc/my.cnf.d/mariadb-server.cnf
fi
if [ ! -d /etc/systemd/system/mariadb.service.d/ ];then
	mkdir /etc/systemd/system/mariadb.service.d/
cat<<@ > /etc/systemd/system/mariadb.service.d/limits.conf
[Service]
LimitNOFILE=10000
@
fi
systemctl stop mariadb
systemctl start mariadb
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL start" > $_stat;exit;fi
cat<<@
$(tput setaf 1)$(tput setab 7)MariaDBの管理パスワードを設定します.
以下のように答えるとよいぞ:
	Enter current passwd: (return)
	Set root password: Y
	New password, Re-enter new password お好きなパスワード入力
	...
	Remove Anonymous Users: Y
	Disallow root login remotey : Y
	Remove test database : Y
	Reload priviledge talbes now: Y
パスワードは覚えときんしゃい.
(HIT ENTER KEY)$(tput sgr0)
@
read _ANS
mysql_secure_installation
echo "$(tput setaf 1)$(tput setab 7)さっきのよ:$(tput sgr0)"
(
mysql -u root -p <<@
drop user if exists 'slurm'@'localhost';
drop user if exists 'slurm'@'`hostname`';
drop database if exists slurm_acct_db;
create user 'slurm'@'localhost' identified by 'password';
create user 'slurm'@'`hostname`' identified by 'password';
create database slurm_acct_db;
grant all on slurm_acct_db.* to 'slurm'@'localhost';
grant all on slurm_acct_db.* to 'slurm'@'`hostname`';
select user,host,password,plugin from mysql.user;
@
)|oneline
_RPM=( `dnf list installed mariadb-server|tail -1` )
systemctl restart mariadb
systemctl enable mariadb
	echo "OK ${_RPM[1]}" |oneline
	echo "OK ${_RPM[1]}" > $_stat
