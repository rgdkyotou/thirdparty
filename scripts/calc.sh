#!/bin/bash
_repo=$2
_ver=$3
_src=`basename $_repo .git`
. scripts/oneline.sh
pushd $1						>&	/dev/null
git_download $_repo $_ver
pushd $_src						>&	/dev/null
sed	-i".org"\
	-e 's@^EXTRA_CFLAGS=.*@EXTRA_CFLAGS=-Wimplicit-fallthrough=0@'\
	-e 's@^BINDIR=.*@BINDIR=/usr/local/bin@'\
	-e 's@^INCDIR=.*@INCDIR=/usr/local/include@'\
	-e 's@^LIBDIR=.*@LIBDIR=/usr/local/lib@'\
	-e 's@^TERMCONTROL=.*@TERMCONTROL=-DUSE_TERMIOS@'\
	-e 's@^MANDIR=.*@MANDIR=/usr/local/share/man/man1@'\
	-e 's@^CALC_SHAREDIR=.*@CALC_SHAREDIR=/usr/local/share/calc@'\
	Makefile.ship
sed\
	-e 's@^CCWERR=.*@CCWERR=@'\
	Makefile	> Makefile.local
make -f Makefile.local install 2>&1 |oneline
if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "かるくOK"				2>&1	|oneline
	echo "OK $_ver" > $_stat
