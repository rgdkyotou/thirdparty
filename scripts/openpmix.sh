#!/bin/bash
#	build basefolder repositoy 
_repo=$2
_ver=$3
_src=`basename $_repo`
_src=${_src%.*}
. scripts/oneline.sh
pushd $1					>&	/dev/null
if [ ! -d $_src ];then
	git clone $_repo			2>&1	|oneline
	echo "Setting version 3.1.4"		2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL download" > $_stat;exit;fi
	pushd $_src				>&	/dev/null
	git checkout -b $_ver refs/tags/$_ver	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL checkout" > $_stat;exit;fi
	popd					>&	/dev/null
fi
pushd $_src					>&	/dev/null
if [ -f Makefile ];then
	make  	distclean	2>&1 |oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL clean" > $_stat;exit;fi
fi
./autogen.pl			2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL automake" > $_stat;exit;fi
./configure --prefix=$_prefix/pmix	2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL configure" > $_stat;exit;fi
make				2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL make" > $_stat;exit;fi
make install				2>&1	|oneline
	if [ ${PIPESTATUS[0]} -ne 0 ];then echo "FAIL install" > $_stat;exit;fi
	echo "ぴみくすOK" 	2>&1	|oneline
	echo "OK $_ver" 	> $_stat
