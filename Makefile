#---------------------------------------------------------------
#FDLAB free software installer for CentOS8
#
#	Sun Nov  3 14:53:01 JST 2019	Ver1.0	ダウンロードはしねえよ今んとこ. 多分ライセンス違反だから
#	Mon Dec 23 11:20:15 JST 2019	Ver1.1	いやもうgitで全部ダウンロードした方が簡単じゃね？
#---------------------------------------------------------------
#	計算クラスタを構成しているEthernetのデバイス名
#		bond0, enp20f0 とかその辺
ETHER=bond0
#	SLURMのサーバーアドレス
RMS_SERVER=10.249.229.196
#	本当のDNSサーバー
DNS_TRUE=( 10.224.253.1 10.224.254.1 )
#	なんちゃってDNSサーバー(FreeIPAのサーバーさんたち)
DNS_FAKE=( 10.249.229.246 10.249.229.223 10.249.229.225 10.249.229.224 )
#---------------------------------------------------------------
all:prepare gsl vtk cgal calc nkf boost-mpi cryptopp lapack log4cplus mysqlpp php plotutils GraphicsMagick daily gmpfrxx mpreal fftw
blas:prepare
	@scripts/ifnot $@	dnf install -y blas
boost:prepare python-devel
	@scripts/ifnot $@	scripts/boost.sh CLUSTER/BASE https://github.com/boostorg/boost.git boost-1.73.0
boost-mpi:prepare boost ompi
	@scripts/ifnot $@	scripts/boost-mpi.sh CLUSTER/BASE https://github.com/boostorg/boost.git boost-1.73.0
calc:prepare
	@scripts/ifnot $@	scripts/calc.sh CLUSTER/LIB https://github.com/lcn2/calc.git 2.12.7.2
cgal:cmake blas gmp mpfr boost cmake eigen ntl
	@scripts/ifnot $@	scripts/CGAL.sh CLUSTER/LIB https://github.com/CGAL/cgal.git releases/CGAL-5.0.3
#	@scripts/ifnot $@	scripts/CGAL.sh CLUSTER/LIB https://github.com/CGAL/cgal.git releases/CGAL-4.13.2
cmake:prepare
	@scripts/ifnot $@	dnf install -y cmake
cryptopp:prepare
	@scripts/ifnot $@	scripts/cryptopp.sh CLUSTER/LIB https://github.com/weidai11/cryptopp.git CRYPTOPP_8_2_0
eigen:prepare
	@scripts/ifnot $@	scripts/eigen.sh CLUSTER/LIB https://gitlab.com/libeigen/eigen.git 3.3.7
gfortran:prepare
	@scripts/ifnot $@	dnf install -y gcc-gfortran
python-devel:prepare
	@scripts/ifnot $@	dnf install -y python36-devel python36
gmp:prepare
	@scripts/ifnot $@	dnf install -y gmp-devel gmp
gmpfrxx:prepare
	@scripts/ifnot $@	scripts/gmpfrxx.sh CLUSTER/LIB http://math.berkeley.edu/~wilken/code/gmpfrxx/gmpfrxx.zip zip gmpfrxx
GraphicsMagick:prepare lzip mjpeg mpeg2
	@scripts/ifnot $@	scripts/GM.sh CLUSTER/LIB https://sourceforge.net/projects/graphicsmagick/files/graphicsmagick/1.3.34/GraphicsMagick-1.3.34.tar.lz tar.lz GraphicsMagick
gsl:prepare
	@scripts/ifnot $@	dnf install -y gsl-devel gsl
httpd:prepare
	@scripts/ifnot $@	dnf install -y httpd-devel
ipa-client:
	@scripts/ifnot $@	scripts/ipa-client.sh $(ETHER) ipa-client
jpeg:prepare
	@scripts/ifnot $@	scripts/jpeg.sh CLUSTER/LIB http://www.ijg.org/files/jpegsrc.v9d.tar.gz tar.gz jpeg
lapack:prepare
	@scripts/ifnot $@	dnf install -y lapack
libcurl:prepare
	@scripts/ifnot $@	dnf install -y libcurl-devel libcurl
libevent-devel:prepare
	@scripts/ifnot $@	dnf install -y libevent-devel libevent
libpng:prepare
	@scripts/ifnot $@	dnf install -y libpng-devel libpng
libopengl:prepare
	@scripts/ifnot $@	dnf install -y libglvnd-opengl libglvnd-devel mesa-libGL mesa-libGL-devel libglvnd
libXaw:prepare
	@scripts/ifnot $@	dnf install -y libXaw-devel libXaw
libXt:prepare
	@scripts/ifnot $@	dnf install -y libXt-devel
log4cplus:prepare
	@scripts/ifnot $@	scripts/config_make.sh CLUSTER/LIB https://sourceforge.net/projects/log4cplus/files/log4cplus-stable/2.0.4/log4cplus-2.0.4.tar.xz tar.xz log4cplus
lzip:prepare
	@scripts/ifnot $@	scripts/config_make.sh CLUSTER/LIB http://download.savannah.gnu.org/releases/lzip/lzip-1.21.tar.gz tar.gz lzip
mailx:prepare
	@scripts/ifnot $@	dnf install -y mailx
maria-devel:prepare
	@scripts/ifnot $@	dnf install -y mariadb-devel
maria:prepare selinux
	@scripts/ifnot $@	scripts/maria.sh maria
mjpeg:prepare jpeg
	@scripts/ifnot $@	scripts/config_make.sh CLUSTER/LIB https://sourceforge.net/projects/mjpeg/files/mjpegtools/2.2.1/mjpegtools-2.2.1.tar.gz tar.gz mjpeg
mpeg2:prepare
	@scripts/ifnot $@	scripts/mpeg2.sh CLUSTER/LIB https://fossies.org/linux/misc/old/mpeg2vidcodec_v12.tar.gz tar.gz mpeg2
mpfr:prepare
	@scripts/ifnot $@	dnf install -y mpfr mpfr-devel
mpreal:prepare
	@scripts/ifnot $@	scripts/mpreal.sh CLUSTER/LIB https://github.com/advanpix/mpreal.git mpfrc++-3.6.7
munge:prepare openssl
	@scripts/ifnot $@	scripts/munge.sh munge
munge.key.create:prepare
	@dd if=/dev/random bs=1 count=1024 > rc/munge.key
munge.key.copy:prepare
	@echo "$$(tput setaf 1)$$(tput setab 7)あんたのRMSサーバー[$(RMS_SERVER)]のキーをコピーします$$(tput sgr0)"
	scp ${RMS_SERVER}:\{/etc/munge/munge.key,/usr/local/etc/slurm.uid,/root/.ssh/id_rsa.pub} rc/
	@cat rc/id_rsa.pub >> /root/.ssh/authorized_keys
	@chmod og-rw /root/.ssh/authorized_keys
mysqlpp:prepare maria-devel
	@scripts/ifnot $@	scripts/config_make.sh CLUSTER/LIB https://tangentsoft.com/mysqlpp/releases/mysql++-3.2.5.tar.gz tar.gz mysqlpp
nkf:prepare
	@scripts/ifnot $@	scripts/simple_make.sh CLUSTER/LIB http://scm.osdn.net/gitroot/nkf/nkf.git v2_1_3
ntl:prepare
	@scripts/ifnot $@	scripts/ntl.sh CLUSTER/LIB https://shoup.net/ntl/ntl-10.3.0.tar.gz tar.gz ntl
ompi:prepare gfortran cmake slurm openpmix
	@scripts/ifnot $@	scripts/ompi.sh CLUSTER/BASE https://github.com/open-mpi/ompi.git v4.0.2
openssl:prepare
	@scripts/ifnot $@	dnf install -y openssl-devel
pam:prepare
	@scripts/ifnot $@	dnf install -y pam-devel
php:prepare httpd
	@scripts/ifnot $@	dnf install -y php php-devel php-pdo php-mysqlnd php-mbstring php-gd php-pear php-json
plotutils:prepare libpng libXaw texinfo
	@scripts/ifnot $@	scripts/plotutils.sh CLUSTER/LIB https://sugimoto605@bitbucket.org/sugimoto605/gnu-plotutils-fd-patch.git 2.6.fd7 plotutils
openpmix:libevent-devel
	@scripts/ifnot $@	scripts/openpmix.sh CLUSTER/BASE https://github.com/openpmix/openpmix.git v3.1.4
prepare:
	@if [ ! -d log ];then mkdir log;fi
	@if [ ! -d stat ];then mkdir stat;fi
	@if [ ! -d CLUSTER ];then mkdir CLUSTER;fi
	@if [ ! -d CLUSTER/BASE ];then mkdir CLUSTER/BASE;fi
	@if [ ! -d CLUSTER/LIB ];then mkdir CLUSTER/LIB;fi
readline:prepare
	@scripts/ifnot $@	dnf install -y readline-devel
kvm:prepare
	@scripts/ifnot $@	scripts/kvm.sh	kvm
selinux:prepare
	@scripts/ifnot $@	scripts/selinux.sh	selinux
slurm:prepare munge maria httpd mailx readline pam libcurl boost openpmix
	@scripts/ifnot $@	scripts/slurm.sh CLUSTER/BASE https://github.com/SchedMD/slurm.git slurm-19-05-4-1
slurm-info:slurm-client
	@echo "Information required in the slurm.conf is:"
	@slurmd -C
slurm-client:prepare kvm slurm boost-mpi
	@scripts/ifnot $@	scripts/slurm-client.sh slurm-client slurm-19-05-4-1
nfs-export:prepare slurm-client
	@scripts/ifnot $@	scripts/nfs-export.sh	nfs-export $(ETHER)
slurm-server:prepare maria slurm boost-mpi
	@scripts/ifnot $@	scripts/slurm-server.sh slurm-server slurm-19-05-4-1
daily:
	@scripts/ifnot daily2	scripts/daily.sh daily
texinfo:prepare
	@scripts/ifnot $@	dnf --enablerepo=powertools install -y texinfo
vtk:prepare cmake boost libXt libopengl
	@scripts/ifnot $@	scripts/VTK.sh CLUSTER/LIB https://gitlab.kitware.com/vtk/vtk v8.2.0
zlib:prepare
	@scripts/ifnot $@	dnf install -y zlib zlib-devel
fftw:prepare
	@scripts/ifnot $@	dnf install -y fftw fftw-devel
