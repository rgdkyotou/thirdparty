#!/bin/bash
#Prolog is run by root with no output
SCRATCH=/local/disk0/tmp/$SLURM_JOB_ID
mkdir -p $SCRATCH
chown $SLURM_JOB_USER $SCRATCH
PATH=$PATH:/usr/local/sbin
exit 0
